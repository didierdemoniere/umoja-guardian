# @umoja/guardian

simple but powerful access control library
 
Features:
* small API surface 3 functions only: (is, allow, can)
* functionnal
* role based
* multi-role
* rights inheritance
* ABAC compatible
* dead simple
* small af
* 1 minute learning curve
* 100% tested
* 100% documented
* typed (typescript)

### Example
#### ACL.js
```js
import * as Guardian from '@umoja/guardian'
// or const Guardian = require('@umoja/guardian')

const heimdall = Guardian.create();
heimdall.is('guest', (user) => !user)
// no user object it's a guest
heimdall.is('member', (user) => !!user)
// a user object it's a member
heimdall.is('writer', (user) => !!user && user.is_writer )
// a user object and a `is_writer` field set to true it's a writer
// note: 'writer' role naturally inherits rights from 'member' role

heimdall.allow('guest', 'get', 'Posts')
// a guest can now access Posts
heimdall.allow('member', 'get', 'Posts')
heimdall.allow('member', 'create', 'Comments')
// a member can now acces Posts and create new Comments
heimdall.allow('writer', 'create', 'Posts')
// a writer can now create new Posts

export const can = heimdall.can
// or module.exports = { can: heimdall.can }
```

#### index.js
```js
import { can } from './ACL'
// or const can = require('./ACL').can

can('Posts', 'get', null)
// => true
can('Posts', 'create', null)
// => false
can('Posts', 'create', {id: 10})
// => false
can('Comments', 'create', {id: 10})
// => true
can('Posts', 'create', {id: 2, is_writer: true})
// => true

// can is curried
const canPosts = can('Posts')
const canPostsCreate = canPosts('create')

canPostsCreate({id: 2, is_writer: true})
// => true
```