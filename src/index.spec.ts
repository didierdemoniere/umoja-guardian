// tslint:disable:no-expression-statement
import test from 'ava';
import * as Guardian from './index';

test('no permission by default', t => {
  const guardian = Guardian.create<any>();

  t.is(guardian.can(null, 'get', 'Posts'), false);
  t.is(guardian.can(null, 'create', 'Posts'), false);
  t.is(guardian.can(null, 'create', 'Comments'), false);
  t.is(guardian.can({ id: 2, is_writer: true }, 'create', 'Posts'), false);
});

test('simple - 1 permission', t => {
  const guardian = Guardian.create<any>();

  guardian.is('guest', user => !user);

  guardian.allow('guest', 'get', 'Posts');

  t.is(guardian.can(null, 'get', 'Posts'), true);
  t.is(guardian.can(null, 'create', 'Posts'), false);
  t.is(guardian.can(null, 'create', 'Comments'), false);
  t.is(guardian.can({ id: 2, is_writer: true }, 'create', 'Posts'), false);
});

test('ABAC & inheritance', t => {
  interface User {
    id: number;
    is_writer?: boolean;
  }

  const guardian = Guardian.create<User | void>();
  guardian.is('guest', user => !user);
  guardian.is('member', user => !!user);
  guardian.is('writer', user => !!user && user.is_writer);

  // anyone can get Posts
  guardian.allow('*', 'get', 'Posts');
  guardian.allow('member', ['create', 'update'], 'Comments');
  guardian.allow('writer', ['create', 'update'], 'Posts');

  t.is(guardian.can(null, 'get', 'Posts'), true);
  t.is(guardian.can(null, 'create', 'Posts'), false);
  t.is(guardian.can(null, 'update', 'Posts'), false);
  t.is(guardian.can({ id: 10 }, 'create', 'Posts'), false);
  t.is(guardian.can({ id: 10 }, 'update', 'Posts'), false);
  t.is(guardian.can({ id: 10 }, 'create', 'Comments'), true);
  t.is(guardian.can({ id: 10 }, 'update', 'Comments'), true);
  // writer rights
  t.is(guardian.can({ id: 2, is_writer: true }, 'create', 'Posts'), true);
  t.is(guardian.can({ id: 2, is_writer: true }, 'update', 'Posts'), true);
  // member rights
  t.is(guardian.can({ id: 2, is_writer: true }, 'create', 'Comments'), true);
  t.is(guardian.can({ id: 2, is_writer: true }, 'update', 'Comments'), true);
});
