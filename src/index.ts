import { Forbidden, NotFound } from '@feathersjs/errors';

interface Roles<T> {
  [key: string]: (user: T) => boolean;
}

interface Rule {
  role: string;
  action: string;
  ressource: string;
}

export type Guardian<T> = {
  allow: {
    (role: string, action: string, ressource: string): void;
    (role: string, actions: string[], ressource: string): void;
  };
  can: (user: T, action: string, ressource: string) => boolean;
  is: (name: string, detect: (user: T) => boolean) => void;
  isProtected: (ressource: string) => boolean;
};

/**
 * factory for the 4 related functions that manage access control
 *
 * @returns  { is, allow, can }
 */
export function create<T>(): Guardian<T> {
  const roles: Roles<T> = Object.create(null);
  const rules: Rule[] = [];
  const ressourceMap: { [ressource: string]: boolean } = {};

  /**
   * register a role with a detect function to know for a given user object
   * if the role apply.
   *
   * @param name    role name.
   * @param detect  detect if the role apply.
   * @returns       void
   */
  function is(name: string, detect: (user: T) => boolean) {
    roles[name] = detect;
  }

  /**
   * gives permissions to roles on ressources
   *
   * @param name        role name.
   * @param actions     allowed action/s
   * @param ressource   ressource name
   * @returns           void
   */
  function allow(role: string, action: string, ressource: string): void;
  function allow(role: string, actions: string[], ressource: string): void;
  function allow(
    role: string,
    actions: string | string[],
    ressource: string
  ): void {
    ressourceMap[ressource] = true;

    if (!Array.isArray(actions)) {
      rules.push({ role, action: actions, ressource });
    } else {
      actions.forEach(action => {
        rules.push({ role, action, ressource });
      });
    }
  }

  /**
   * check if a given ressource is protected
   *
   * @param ressource
   */
  function isProtected(ressource: string): boolean {
    return !!ressourceMap[ressource];
  }

  /**
   * check if a given user object is allowed perform an action
   *
   * @param user        user object
   * @param action      action the user want to do
   * @param ressource   ressource of perfomed action
   * @returns           boolean
   */
  function can(user: T, action: string, ressource: string): boolean {
    if (!isProtected(ressource)) {
      return false;
    }

    const userRoles = Object.keys(roles).filter(key => roles[key](user));

    return rules.some(
      rule =>
        (rule.action === '*' || rule.action === action) &&
        (rule.ressource === '*' || rule.ressource === ressource) &&
        (rule.role === '*' || userRoles.includes(rule.role))
    );
  }

  return { allow, can, is, isProtected };
}

export function watchTower<T>(guardian: Guardian<T>) {
  return (req, _, next) => {
    const parts = req.path.split('/').filter(n => n);
    const hasId = parts.length > 1;
    const ressource = parts[0];
    const action =
      req.method === 'GET'
        ? hasId
          ? 'get'
          : 'find'
        : req.method === 'POST'
        ? 'create'
        : req.method === 'PUT'
        ? 'update'
        : req.method === 'PATCH'
        ? 'patch'
        : req.method === 'DELETE'
        ? 'remove'
        : false;

    if (!action) {
      return next();
    } else {
      if (!guardian.isProtected(ressource)) {
        return next(new NotFound());
      } else if (!guardian.can(req.user, action, ressource)) {
        return next(new Forbidden());
      } else {
        return next();
      }
    }
  };
}
