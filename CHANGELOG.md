# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="4.1.0"></a>
# [4.1.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v4.0.0...v4.1.0) (2020-04-16)


### Features

* **midleware:** add middleware factory ([75d2a22](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/commit/75d2a22))



<a name="4.0.0"></a>
# [4.0.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v3.0.3...v4.0.0) (2019-07-14)


### build

* **prod package:** generate production package ([cb214db](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/commit/cb214db))


### BREAKING CHANGES

* **prod package:** package cannot be built directly from source anymore



<a name="3.0.3"></a>
## [3.0.3](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v3.0.2...v3.0.3) (2019-05-21)



<a name="3.0.2"></a>
## [3.0.2](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v3.0.1...v3.0.2) (2019-05-21)



<a name="3.0.1"></a>
## [3.0.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v3.0.0...v3.0.1) (2018-10-06)



<a name="3.0.0"></a>
# [3.0.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v2.0.1...v3.0.0) (2018-09-18)


### Features

* **api:** remove currying for a simpler and more obvious api ([d96e549](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/commit/d96e549))


### BREAKING CHANGES

* **api:** can function arguments order



<a name="2.0.1"></a>
## [2.0.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v2.0.0...v2.0.1) (2018-09-18)



<a name="2.0.0"></a>
# [2.0.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/compare/v1.0.1...v2.0.0) (2018-09-17)


### Features

* **index.ts:** can function arguments is now in reverse order and the function is now curried ([6734e37](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/guardian/commit/6734e37))


### BREAKING CHANGES

* **index.ts:** - can
